<?php
require('bootstrap.php')
?>
    <form class="passegAdd" method="POST" action="Testdirect.php" enctype="multipart/form-data" >
        <div class="col-lg-12 col-xs-12">
            <label class="nbox-span">Объект посещения<span class="req-field">*</span></label>
            <div class="select_ico">
                <select class="form-controlre rfield" name="00012OBJECTS" id="visiting_object" onchange="goInfoSelect('#visiting_object','#obekt_posesheniya'); getKppForPass6_RUK(); offMap()" style="border-color: rgb(57, 181, 74);"><option>Варианты</option><option id="0" value="248399">АБК Курако 33 (Центральный вход)</option><option id="1" value="184316">Новокузнецкая материальная база</option><option id="2" value="248398">Учебный центр РУК</option><option id="3" value="184317">ЦСР Курако 35</option></select>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-lg-4">
                <label class="nbox-span">Фамилия<span class="req-field">*</span></label>
                <input class="ntext-box rfield onlyRu" type="text" name="DRIVER_FIRSTNAME" id="DRIVER_FIRSTNAME"
                       onchange="goInfo('#DRIVER_FIRSTNAME','#familia_voditelya'); CheckAccessUser();" maxlength="60"/>
            </div>

            <div class="col-xs-12 col-lg-4">
                <label class="nbox-span">Имя<span class="req-field">*</span></label>
                <input class=" ntext-box rfield onlyRu" type="text" name="DRIVER_MIDDLENAME" id="DRIVER_MIDDLENAME"
                       onchange="goInfo('#DRIVER_MIDDLENAME','#imya_voditelya'); CheckAccessUser();" maxlength="60"/>
            </div>

            <div class="col-xs-12 col-lg-4">
                <label class="nbox-span">Отчество<span class="req-field">*</span></label>
                <input class="ntext-box rfield onlyRu" type="text" name="DRIVER_LASTNAME" id="DRIVER_LASTNAME"
                       onchange="goInfo('#DRIVER_LASTNAME','#ot4estvo_voditelya'); CheckAccessUser();" maxlength="60"/>
            </div>

            <div class="col-xs-12 col-lg-4">
                <label class="nbox-span">Дата рождения<span class="req-field">*</span></label>
                <input class="js-date maska_test ntext-box rfield js-date-day" type="text" id="datepicker10" name="DRIVER_BIRTHDAY"
                       data-type="not-future"
                       onchange="goInfo('#datepicker10','#datarojdeni_voditelya'); CheckAccessUser();" maxlength="60"/>
            </div>

            <div class="col-lg-6 col-xs-12">
                <label class="nbox-span">Дата посещения<span class="req-field">*</span></label>
                <input class="js-date maska_test ntext-box rfield" type="text" id="dateр" name="date_visit" data-type="vizit" ;" maxlength="60"/>
            </div>

        </div>


        <div class="nblock2 send_agreement flex_block space_between mobile_align_row_nblock2">

            <a href="#" onclick="send_data()">send data btn </a>
        </div>
        <div id="request"> Look here.</div>
    </form>

    <script
            src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script>
        var send_data = function() {
            $.ajax({
                type: 'post',
                url: 'Testdirect.php',
                data: $('.passegAdd').serialize(),
                success: function (result) {
                    console.log( result );
                    $('#request').html( result );
                }
            });
            return true;
        }
        $("#visiting_object").change(function(){
            if($(this).val() == 248399) {
                return true;
            }
            else {
                send_data();
            }
        });

    </script>

<?